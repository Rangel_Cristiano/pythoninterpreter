﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonInterpreter
{
    static class Gramatic
    {
        public static readonly string[] OPERATORS = new string[]
        {
            "id", "+", "-", "*", "**", "/", "%", "//", "&", "|", "==","!=", ">>", 
            "<<", "<", ">", "<=", ">=", "~", "^", "--", "++", ":", "(", ")",
            "'", ".", ",", "=", "[", "]"
        };

        public static readonly string[] WORDS = new string[]
        {
            "int", "float", "string", "if", "elif", "else", "while", "for", "bin",
            "hex", "oct", "dec", "and", "or", "def", "return", "in", "not"
        };
    }

}
