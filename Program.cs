﻿using System;
using System.Collections;
using System.IO;

//ctrl + f5 pra executar

namespace PythonInterpreter
{
    class Program
    {
        static ArrayList values = new ArrayList();
        static ArrayList lin = new ArrayList();
        static ArrayList col = new ArrayList();
        static ArrayList lexList = new ArrayList();

        static void Main(string[] args)
        {
            Lexicale lexicale = new Lexicale();

            int counterLine = 0;
            string line;

            System.IO.StreamReader fRead = new System.IO.StreamReader(@"C:\Rangel\entrada.txt");
            StreamWriter fWrite = new StreamWriter(@"C:\Rangel\saida.txt");

            while ((line = fRead.ReadLine()) != null)
            {
                //System.Console.WriteLine(line);

                counterLine++;

                if (line != null && line.Length > 0)
                {
                    string lex;
                     while ((lex = lexicale.analyse(line)) != null)
                    {
                        //Console.WriteLine("TOKEN: " + lexicale.Token + " | LEXEMA: " + lex + " | Linha: " + counterLine
                        //    + "  COLUNA: " + (lexicale.Position - lex.Length + 1));
                        
                        fWrite.WriteLine("TOKEN: " + lexicale.Token + " | LEXEMA: " + lex + " | Linha: " + counterLine
                            + "  COLUNA: " + (lexicale.Position - lex.Length + 1));

                        values.Add(lexicale.Token == 1 ? "id" : lex);
                        lin.Add(counterLine);
                        col.Add(lexicale.Position - lex.Length + 1);
                        lexList.Add(lex);
                    }
                }
            }

            new Syntax().analyse(values, lin, col);
            fWrite.WriteLine("");
            fWrite.WriteLine("________________________________________________________");
            fWrite.WriteLine("");
            fWrite.WriteLine("C3E");
            fWrite.WriteLine("");

            new Semantic().analyse(values, lin, col, lexList, fWrite);

            fRead.Close();
            fWrite.Close();


            Console.ReadKey();
        }
    }
}
