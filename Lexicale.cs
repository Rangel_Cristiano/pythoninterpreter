﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonInterpreter
{
    class Lexicale
    {
        private string findOperator(char c)
        {
            string find = Gramatic.OPERATORS.FirstOrDefault(Convert.ToString(c).Equals);
            if (find != null) _token = Array.IndexOf(Gramatic.OPERATORS, find) + 1;

            if (find == null)
            {
                //Console.WriteLine("Erro léxico: encontrou o caracter " + c + " na posição "
                  //  + Convert.ToString(pos + 1));
            }

            return find;
        }
        
        int pos = 0;
        int _token = 0;

        public string analyse(string code)
        {
            string find, word = null;
            while (pos < code.Length)
            {
                string lex = null;

                char c = code[pos];
                if (c != ' ')
                {
                    if (!Char.IsLetter(c) && !Char.IsNumber(c) && !c.Equals('_'))
                    {
                        if (!(pos + 1 == code.Length))
                        {
                            find = Gramatic.OPERATORS.FirstOrDefault((Convert.ToString(c) + Convert.ToString(code[pos + 1])).Equals);
                            if (find == null)
                            {
                                lex = findOperator(c);
                            }
                            else
                            {
                                lex = find;
                                _token = Array.IndexOf(Gramatic.OPERATORS, find) + 1;

                                pos++;
                            }
                        }
                        else
                        {
                            lex = findOperator(c);
                        }
                    }
                    else
                    {
                        word = word + c;

                        if (pos + 1 == code.Length || (!Char.IsLetter(code[pos + 1]) &&
                            !Char.IsNumber(code[pos + 1]) && !code[pos + 1].Equals('_')))
                        {

                            lex = Gramatic.WORDS.FirstOrDefault(word.Equals);

                            if (lex != null)
                            {
                                _token = Array.IndexOf(Gramatic.WORDS, lex) + Gramatic.OPERATORS.Length + 1;
                            }
                            else
                            {
                                lex = word;

                                _token = 1;
                            }

                            word = null;
                        }
                    }
                }       

                pos++;

                if (lex != null) return lex;
            }

            pos = 0;

            return null;
        }

        // ULTIMA POSIÇÂO DO LEXEMA NO ARQUIVO
        public int Position
        {
            get
            {
                return pos;
            }
        }

        // É A POSIÇÃO DO LEXAMA NO ARRAY
        public int Token
        {
            get
            {
                return _token;
            }
        }
    }
}
