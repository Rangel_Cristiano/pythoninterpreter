﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace PythonInterpreter
{
    class Semantic
    {
        bool IsCorrect = true;
        ArrayList values, lin, col, lexList;
        ArrayList defs = new ArrayList();
        ArrayList fors = new ArrayList();
        ArrayList labelfors = new ArrayList();
        ArrayList labelIniciofors = new ArrayList();
        ArrayList closeFors = new ArrayList();
        ArrayList ifs = new ArrayList();
        ArrayList closeIfs = new ArrayList();
        ArrayList labelIfs = new ArrayList();
        ArrayList whiles = new ArrayList();
        ArrayList labelWhiles = new ArrayList();
        ArrayList labelInicioWhiles = new ArrayList();
        ArrayList trys = new ArrayList();
        ArrayList variables = new ArrayList();
        ArrayList types = new ArrayList();
        StreamWriter fwrite;
        private int i;

        public void analyse(ArrayList v, ArrayList l, ArrayList c, ArrayList lx, StreamWriter f)
        {
            string code = "";

            fwrite = f;
            values = v;
            lin = l;
            col = c;
            lexList = lx;

            for (i = 0; i < values.Count; i++)
            {
                if (i == 0 || !lin[i - 1].Equals(lin[i]))
                {                    
                    if (whiles.Count > 0 && labelWhiles.Count > 0)
                    {
                        if (!values[i].Equals("else") && !values[i].Equals("elif"))
                        {
                            var last = labelWhiles.Count - 1;

                            if (whiles[last].Equals(col[i]))
                            {
                                System.Console.WriteLine("goto " + labelInicioWhiles[last]);
                                fwrite.WriteLine("goto " + labelInicioWhiles[last]);

                                System.Console.WriteLine(labelWhiles[last] + " :");
                                fwrite.WriteLine(labelWhiles[last] + " :");

                                whiles.RemoveAt(last);
                                labelWhiles.RemoveAt(last);
                            }                        
                        }
                    }
                    else if (ifs.Count > 0 && labelIfs.Count > 0)
                    {
                        if (!values[i].Equals("else") && !values[i].Equals("elif"))
                        {
                            var last = labelIfs.Count - 1;

                            if (ifs[last].Equals(col[i]))
                            {
                                System.Console.WriteLine(labelIfs[last] + " :");
                                fwrite.WriteLine(labelIfs[last] + " :");

                                ifs.RemoveAt(last);
                                labelIfs.RemoveAt(last);

                            }
                        }
                    }
                    else if (fors.Count > 0 && labelfors.Count > 0)
                    {
                        if (!values[i].Equals("else") && !values[i].Equals("elif"))
                        {
                            var last = labelfors.Count - 1;

                            if (fors[last].Equals(col[i]))
                            {
                                System.Console.WriteLine("goto " + labelIniciofors[last]);
                                fwrite.WriteLine("goto " + labelIniciofors[last]);

                                System.Console.WriteLine(labelfors[last] + " :");
                                fwrite.WriteLine(labelfors[last] + " :");

                                fors.RemoveAt(last);
                                labelfors.RemoveAt(last);
                            }
                        }
                    }

                    if (defs.Count > 0)
                    {
                        if (defs.Contains(lexList[i]))
                        {
                            var nome = lexList[i];
                            i++;
                            if (values[i].Equals("("))
                            {
                                i++;
                                while (!values[i].Equals(")"))
                                {
                                    if (values[i].Equals("id"))
                                    {
                                        Console.WriteLine(lexList[i]);
                                        i++;
                                        if (values[i].Equals(","))
                                        {
                                            Console.WriteLine(lexList[i]);
                                            i++;
                                        }
                                        else
                                        {
                                            if (!values[i].Equals(")"))
                                            {
                                                Error(i);
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine(lexList[i]);
                                            }
                                        }
                                    }
                                }

                                Console.WriteLine("goto " + nome);
                                fwrite.WriteLine("goto " + nome);

                                i++;

                                if (i > lexList.Count - 1) break;
                            }

                        }                  
                    }

                    if (values[i].Equals("int") || values[i].Equals("float") || values[i].Equals("string"))
                    {
                        var type = values[i].ToString();
                        types.Add(type);
                        Console.WriteLine(lexList[i]);

                        i++;
                        if (values[i].Equals("id"))
                        {
                            Console.WriteLine(lexList[i]);
                            var id = lexList[i].ToString();
                            variables.Add(id);

                            createTemp(id);

                            i++;

                            code = attribution(type, id, code);

                            if (i == -1) break;
                        }
                        else
                        {
                            Error(i);

                            break;
                        }
                    }
                    else if (values[i].Equals("if") || values[i].Equals("while"))
                    {
                        Console.WriteLine(lexList[i]);

                        string whileLabel = null;
                        if (values[i].Equals("while"))
                        {
                            whileLabel = generateLabel("inicio", i);
                            labelInicioWhiles.Add(whileLabel);

                            whiles.Add(col[i]);
                        }
                        else
                        {
                            ifs.Add(col[i]);
                        }

                        code = code + "if ";
                        i++;

                        var regExp = regularExpression();

                        if (i == -1) break;

                        var temp = createTemp("E", i);
                        regExp = temp + " = " + regExp;
                        Console.WriteLine(regExp);
                        fwrite.WriteLine(regExp);
                        temp = temp + " == 0 ";

                        while (values[i].Equals("and") || values[i].Equals("or"))
                        {
                            var value = values[i];
                            if (values[i].Equals("and") || values[i].Equals("or"))
                            {
                                i++;

                                var reg = regularExpression();
                                var tmp = createTemp("E", i);
                                regExp = tmp + " = " + reg;

                                Console.WriteLine(regExp);
                                fwrite.WriteLine(regExp);

                                temp = temp + value + " " + tmp + " == 0 ";

                                if (i == -1) break;
                            }
                        }

                        var labelFalse = generateLabel("LabelFalse", i);
                        if (whileLabel != null)
                        {
                            Console.WriteLine(whileLabel + " : ");
                            fwrite.WriteLine(whileLabel + " : ");
                        }

                        code = code + temp  + "goto " + labelFalse;

                        if (whileLabel != null)
                        {
                            labelWhiles.Add(labelFalse);
                        }
                        else
                        {
                            labelIfs.Add(labelFalse);
                        }

                        if (values[i].Equals(":"))
                        {
                            Console.WriteLine(lexList[i]);
                        }
                        else
                        {
                            Error(i);

                            break;
                        }
                    }
                    else if (values[i].Equals("else") || values[i].Equals("elif"))
                    {
                        Console.WriteLine(lexList[i]);
                        if (ifs.Count > 0 || whiles.Count > 0 || fors.Count > 0)
                        {
                            bool founded = false;

                            if (values[i].Equals("elif"))
                            {
                                for (int j = 0; j < ifs.Count; j++)
                                {
                                    if (ifs[j].Equals(col[i]))
                                    {
                                        founded = true;

                                        var last = labelIfs.Count - 1;

                                        if (ifs[last].Equals(col[i]))
                                        {
                                            System.Console.WriteLine(labelIfs[last] + " :");
                                            fwrite.WriteLine(labelIfs[last] + " :");

                                            ifs.RemoveAt(last);
                                            labelIfs.RemoveAt(last);

                                        }

                                        ifs.Add(col[i]);
                                        code = code + "if ";

                                        i++;


                                        var regExp = regularExpression();
                                        var temp = createTemp("E", i);
                                        regExp = temp + " = " + regExp;
                                        Console.WriteLine(regExp);
                                        fwrite.WriteLine(regExp);

                                        temp = temp + " == 0 ";

                                        if (i == -1) break;

                                        while (values[i].Equals("and") || values[i].Equals("or"))
                                        {
                                            var value = values[i];
                                            if (values[i].Equals("and") || values[i].Equals("or"))
                                            {
                                                i++;

                                                var reg = regularExpression();
                                                var tmp = createTemp("E", i);
                                                regExp = tmp + " = " + reg;

                                                Console.WriteLine(regExp);
                                                fwrite.WriteLine(regExp);

                                                temp = temp + value + " " + tmp + " == 0 ";

                                                if (i == -1) break;

                                                i++;
                                            }
                                        }

                                        var labelFalse = generateLabel("LabelFalse", i);
                                   
                                        code = code + temp + "goto " + labelFalse;
                                        labelIfs.Add(labelFalse);
                                    }
                                }
                            }
                            else
                            {
                                for (int j = 0; j < ifs.Count; j++)
                                {
                                    if (ifs[j].Equals(col[i]))
                                    {
                                        founded = true;

                                        code = code + labelIfs[j] + " : ";
                                        ifs.RemoveAt(j);
                                        labelIfs.RemoveAt(j);

                                        i++;
                                    }
                                }

                                for (int j = 0; j < whiles.Count; j++)
                                {
                                    if (whiles[j].Equals(col[i]))
                                    {
                                        founded = true;

                                        code = code + "goto " + labelInicioWhiles[j] + "\r\n";
                                        code = code + labelWhiles[j] + " : ";

                                        whiles.RemoveAt(j);
                                        labelWhiles.RemoveAt(j);

                                        i++;
                                    }
                                }

                                for (int j = 0; j < fors.Count; j++)
                                {
                                    if (fors[j].Equals(col[i]))
                                    {
                                        founded = true;

                                        code = code + "goto " + labelIniciofors[j] + "\r\n";
                                        code = code + labelfors[j] + " : ";

                                        fors.RemoveAt(j);
                                        labelfors.RemoveAt(j);

                                        i++;
                                    }
                                }
                            }

                            if (!founded)
                            {
                                Error(i);

                                break;
                            }
                            else if (!values[i].Equals(":"))
                            {
                                Error(i);

                                break;
                            }
                            else
                            {
                                Console.WriteLine(lexList[i]);
                            }
                        }
                        else
                        {
                            Error(i + 1);

                            break;
                        }
                    }
                    else if (values[i].Equals("for"))
                    {
                        fors.Add(col[i]);

                        Console.WriteLine(lexList[i]);
                        i++;
                        if (values[i].Equals("id"))
                        {
                            var valueRecebe = lexList[i];
                            Console.WriteLine(lexList[i]);
                            i++;
                            if (values[i].Equals("in"))
                            {
                                Console.WriteLine(lexList[i]);
                                i++;
                                if (values[i].Equals("id"))
                                {
                                    var value = lexList[i];
                                    Console.WriteLine(lexList[i]);
                                    i++;
                                    if (values[i].Equals(":"))
                                    {
                                        var labelFor = generateLabel("inicio", i);
                                        labelIniciofors.Add(labelFor);

                                        Console.WriteLine(lexList[i]);
                                        var labelFalse = generateLabel("LabelFalse", i);

                                        code = code + valueRecebe + ".place = " + value + ".place \r\n";

                                        code = code + "if " + valueRecebe + ".place == 0 goto " + labelFalse;
                                        labelfors.Add(labelFalse);
                                    }
                                    else
                                    {
                                        Error(i);

                                        break;
                                    }
                                }
                                else
                                {
                                    Error(i);

                                    break;
                                }
                            }
                            else
                            {
                                Error(i);

                                break;
                            }

                        }
                        else
                        {
                            Error(i);

                            break;
                        }
                    }
                    else if (values[i].Equals("def"))
                    {
                        Console.WriteLine(lexList[i]);
                        i++;
                        if (values[i].Equals("id"))
                        {
                            var nomeFunc = lexList[i];
                            defs.Add(nomeFunc);
                            Console.WriteLine(lexList[i]);
                            i++;
                            if (values[i].Equals("("))
                            {
                                Console.WriteLine(lexList[i]);
                                i++;
                                while (!values[i].Equals(")"))
                                {
                                    if (values[i].Equals("id"))
                                    {
                                        Console.WriteLine(lexList[i]);
                                        i++;
                                        if (values[i].Equals(","))
                                        {
                                            Console.WriteLine(lexList[i]);
                                            i++;
                                        }
                                        else
                                        {
                                            if (!values[i].Equals(")"))
                                            {
                                                Error(i);
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine(lexList[i]);
                                            }
                                        }
                                    }
                                }

                                i++;
                                if (!values[i].Equals(":"))
                                {
                                    Error(i);
                                    break;
                                }
                                else
                                {
                                    code = code + nomeFunc + ":";
                                    Console.WriteLine(lexList[i]);
                                }
                            }
                            else
                            {
                                Error(i);
                                break;
                            }
                        }
                        else
                        {
                            Error(i);
                            break;
                        }
                    }
                    else if (values[i].Equals("return"))
                    {
                        Console.WriteLine(lexList[i]);
                        i++;
                        if (!values[i].Equals("id"))
                        {
                            Error(i);
                            break;
                        }
                        else
                        {
                            Console.WriteLine("id");

                            string codeOP = null;
                            codeOP = operations(i, code);

                            if (codeOP != null)
                            {
                                code = code + codeOP;
                            }

                            if (i == -1) break;
                        }
                    }
                    else if (values[i].Equals("oct") || values[i].Equals("hex") || values[i].Equals("bin") ||
                        values[i].Equals("dec"))
                    {
                        Console.WriteLine(lexList[i]);
                        i++;
                        if (values[i].Equals("("))
                        {
                            Console.WriteLine(lexList[i]);
                            i++;

                            if (values[i].Equals("id"))
                            {
                                Console.WriteLine(lexList[i]);
                                i++;

                                if (!values[i].Equals(")"))
                                {
                                    Error(i);

                                    break;
                                }
                                else
                                {
                                    Console.WriteLine(lexList[i]);
                                }
                            }
                        }
                        else
                        {
                            Error(i);

                            break;
                        }
                    }
                    else if (values[i].Equals("id"))
                    {
                        Console.WriteLine(lexList[i]);
                        var id = lexList[i];
                        i++;
                        

                        int j = bitTobit(i);

                        if (j == -1)
                        {
                            code = attribution(findType(id.ToString()), id.ToString(), code);
                        }
                        else
                        {
                            i = j;
                        }

                        if (i == -1) break;

                    }
                    else if (values[i].Equals("~"))
                    {
                        i++;
                        Console.WriteLine(lexList[i]);

                        if (values[i].Equals("id"))
                        {
                            Console.WriteLine(lexList[i]);
                        }
                        else
                        {
                            Error(i);

                            break;

                        }
                    }
                    else
                    {
                        Error(i);

                        break;
                    }
                }
                else
                {
                    Error(i);

                    break;
                }

                Console.WriteLine(code);
                fwrite.WriteLine(code);
                code = "";
            }

            if (IsCorrect)
            {
                //se nao tiver codigo embaixo tem que verificar quando acaba pois eu vejo pela coluna do proximo token
                if (whiles.Count > 0 && labelWhiles.Count > 0)
                {
                    var last = labelWhiles.Count - 1;

                    System.Console.WriteLine("goto " + labelInicioWhiles[last]);
                    fwrite.WriteLine("goto " + labelInicioWhiles[last]);

                    System.Console.WriteLine(labelWhiles[last] + " :");
                    fwrite.WriteLine(labelWhiles[last] + " :");

                    whiles.RemoveAt(last);
                    labelWhiles.RemoveAt(last);
                }
                else if (ifs.Count > 0 && labelIfs.Count > 0)
                {
                    var last = labelIfs.Count - 1;

                    System.Console.WriteLine(labelIfs[last] + " :");
                    fwrite.WriteLine(labelIfs[last] + " :");

                    ifs.RemoveAt(last);
                    labelIfs.RemoveAt(last);
                }
                else if (fors.Count > 0 && labelfors.Count > 0)
                {
                    var last = labelfors.Count - 1;

                    System.Console.WriteLine("goto " + labelIniciofors[last]);
                    fwrite.WriteLine("goto " + labelIniciofors[last]);

                    System.Console.WriteLine(labelfors[last] + " :");
                    fwrite.WriteLine(labelfors[last] + " :");

                    fors.RemoveAt(last);
                    labelfors.RemoveAt(last);
                }

            }

            if (IsCorrect)
            {
                Console.WriteLine();
                Console.WriteLine("________________________");
                Console.WriteLine();

                Console.WriteLine("Semantica correta !");
                Console.WriteLine("________________________");
                Console.WriteLine();

            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("________________________");
                Console.WriteLine("Semantica Incorreta !");
                Console.WriteLine("________________________");
                Console.WriteLine();
            }

            Console.WriteLine("Verificar C3E em arquivo txt gerado em C:/Rangel");
        }

        public int bitTobit(int i)
        {
            if (i == -1 || i >= values.Count)
                return i;

            if (values[i].Equals(">>") || values[i].Equals("<<") || values[i].Equals("&") || values[i].Equals("|"))
            {
                i++;
                Console.WriteLine(">> | << | & | |");

                if (values[i].Equals("id"))
                {
                    Console.WriteLine(lexList[i]);

                    return i;
                }
                else
                {
                    Error(i);

                    return -1;
                }
            }

            return -1;
        }

        public string operations(int j, string code)
        {
            if (j == -1 || j >= values.Count)
            {
                return null;
            }
              
            if (values[j].Equals("+") || values[j].Equals("-") || values[j].Equals("*") 
                || values[j].Equals("/") || values[j].Equals("^"))
            {
                var op = lexList[j];
                j++;
                Console.WriteLine(lexList[j]);

                if (values[j].Equals("id"))
                {
                    Console.WriteLine(lexList[j]);

                    bool ok = verifyOpBetweenVars(lexList[j - 2].ToString(), lexList[j].ToString());

                    if (!ok)
                    {
                        System.Console.WriteLine("Tipos da operação não coincidem entre si");

                        Error(i);

                        i = -1;
                    }

                    code = code + " " + op + " " + place(lexList[j].ToString());

                    int k = bitTobit(j + 1);
                    if (k != -1) j = k;

                     
                    i = j;
                    return code;
                }
                else
                {
                    Error(i);

                    i = -1;

                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public string attribution(string type, string id, string code)
        {
            if (values[i].Equals("="))
            {
                Console.WriteLine(lexList[i]);
                i++;

                if (values[i].Equals("id"))
                {
                    Console.WriteLine(lexList[i]);

                    bool ok = verifyOpBetweenVars(lexList[i - 2].ToString(), lexList[i].ToString());

                    if (!ok)
                    {
                        System.Console.WriteLine("Tipo da variável setada nao coincide");

                        Error(i);

                        i = -1;

                        return code;
                    }

                    string placeAux = place(id.ToString());
                    code = code + place(id.ToString()) + " = " + place(lexList[i].ToString());

                    int j = i;
                    while (j != -1)
                    {
                        j++;

                        string codeOP = null;
                        codeOP = operations(j, code);

                        if (codeOP != null)
                        {
                            code = codeOP;
                        }
                        else
                        {
                            break;
                        }

                        j = i;
                    }

                    return code;
                }
                else
                {
                    Error(i);

                    i = -1;
                    return null;
                }
            }
            else if (values[i].Equals("++") || values[i].Equals("--"))
            {
                Console.WriteLine(lexList[i]);

                string t = findType(id);
                if (t == null || !t.Equals("int"))
                {
                    if (t == null)
                    {
                        Console.WriteLine("A variável precisa ser declarada");
                    } 
                    else 
                    {
                       Console.WriteLine("A variável precisa ser do tipo int");
                    }

                    Error(i);

                    return null;
                }

                string tempVar = createTempAndSet(lexList[i - 1].ToString(), i);
                 
                if (values[i].Equals("++"))
                {
                    code = code + lexList[i - 1] + ".place" + " = " + tempVar + " + 1";
                }
                else
                {
                    code = code + lexList[i - 1] + ".place" + " = " + tempVar + " - 1";
                }

                return code;
            }
            else
            {
                i--;
                return code;
            }
        }

        public string regularExpression()
        {
            string codeOP = "";
            if (values[i].Equals("id"))
            {
                Console.WriteLine(lexList[i]);
                codeOP = lexList[i] + ".place ";

                var typeOne = findType(lexList[i].ToString());
                if (typeOne == null)
                {
                    if (IsDigitsOnly(lexList[i].ToString()))
                        typeOne = "int";
                    else
                        typeOne = "string";
                }

                i++;

                var op = operations(i, codeOP);
                codeOP = op == null ? codeOP : codeOP + op;

                if (values[i].Equals("<") || values[i].Equals(">") || values[i].Equals("<=") ||
                    values[i].Equals(">=") || values[i].Equals("==") || values[i].Equals("!="))
                {
                    Console.WriteLine(lexList[i]);
                    codeOP = codeOP + lexList[i] + " ";
                    i++;

                    if (values[i].Equals("id"))
                    {
                        Console.WriteLine(lexList[i]);
                        codeOP = codeOP + lexList[i];

                        var typeTwo = findType(lexList[i].ToString());
                        if (typeTwo == null)
                        {
                            if (IsDigitsOnly(lexList[i].ToString()))
                                typeTwo = "int";
                            else
                                typeTwo = "string";
                        }

                        if (!typeOne.Equals(typeTwo))
                        {
                            codeOP = "";
                            Console.WriteLine("Os tipos das variaveis comparadas sao diferentes");
                            Error(i);

                            return null;
                        }

                        i++;

                        return codeOP;
                    }
                    else
                    {
                        Error(i);

                        return codeOP;
                    }
                }
                else if (values[i].Equals("%"))
                {
                    Console.WriteLine(lexList[i]);
                    codeOP = codeOP + lexList[i] + " ";

                    i++;
                    if (values[i].Equals("id"))
                    {
                        Console.WriteLine(lexList[i]);
                        codeOP = codeOP + lexList[i] + " ";

                        i++;
                        if (values[i].Equals("="))
                        {
                            Console.WriteLine(lexList[i]);
                            codeOP = codeOP + lexList[i] + " ";

                            i++;
                            if (values[i].Equals("id"))
                            {
                                Console.WriteLine(lexList[i]);
                                codeOP = codeOP + lexList[i] + " ";

                                i++;

                                return codeOP;
                            }
                            else
                            {
                                Error(i);
                            }
                        }
                        else
                        {
                            Error(i);
                        }
                    }
                    else
                    {
                        Error(i);
                    }
                }

            }
            else
            {
                Error(i);
            }

            return codeOP;
        }

        public void Error(int index)
        {
            System.Console.WriteLine("ERRO:  Lexema: " + lexList[index] + "  |  Linha: " + lin[index]
                + "  |  Coluna: " + col[index]);

            i = -1;

            IsCorrect = false;
        }

        private void createTemp(string id) {
            System.Console.WriteLine(id + ".place = criaTemp()");
            fwrite.WriteLine(id + ".place = criaTemp()");
        }

        private string createTemp(string id, int i)
        {
            string temp = id + i.ToString();
            System.Console.WriteLine(temp + ".place = criaTemp()");
            fwrite.WriteLine(temp + ".place = criaTemp()");

            return temp + ".place";
        }

        private string createTempAndSet(string id, int i)
        {
            string temp = id + i.ToString();
            System.Console.WriteLine(temp + ".place = criaTemp()");
            fwrite.WriteLine(temp + ".place = criaTemp()");

            System.Console.WriteLine(temp + ".place = " + id + ".place");
            fwrite.WriteLine(temp + ".place = " + id + ".place");

            return temp + ".place";
        }

        private string generateLabel(string label, int pos) {
            System.Console.WriteLine(label + pos.ToString() + " = geraLabel()");
            fwrite.WriteLine(label + pos.ToString() + " = geraLabel()");

            return label + pos.ToString();
        }

        private string place(string id)
        {
            if (findType(id) == null)
            {
                //var criaTemp = "T" + i.ToString() + ".place" + " = " + id + "\n";

                //return criaTemp + "T" + i.ToString() + ".place";

                return id;
            }
            else
            {
                return id + ".place";
            }
        }

        private bool verifyOpBetweenVars(string var1, string var2) 
        {
            var type1 = findType(var1);
            var type2 = findType(var2);

            if (type1 == null)
            {
                if (IsDigitsOnly(var1))
                {
                    type1 = "int";
                }
                else
                {
                    type1 = "string";
                }               
            }


            if (type2 == null)
            {
                if (IsDigitsOnly(var2))
                {
                    type2 = "int";
                }
                else
                {
                    type2 = "string";
                }               
            }


            if (type2.Equals(type1))
            {
                return true;
            }
            else
            {
                return false;
            }           
        }

        private string findType(string variable)
        {
            string type = null;
            for (int i = 0; i < variables.Count; i++)
            {
                if (variables[i].Equals(variable))
                {
                    type = types[i].ToString();
                }
            }

            return type;
        }

        bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }
}
