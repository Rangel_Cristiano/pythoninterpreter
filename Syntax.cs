﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonInterpreter
{
    class Syntax
    {
        bool IsCorrect = true;
        ArrayList values, lin, col;
        ArrayList defs = new ArrayList();
        ArrayList fors = new ArrayList();
        ArrayList ifs = new ArrayList();
        ArrayList whiles = new ArrayList();
        ArrayList trys = new ArrayList();

        public void analyse(ArrayList v, ArrayList l, ArrayList c)
        {
            values = v;
            lin = l;
            col = c;

            for (int i = 0; i < values.Count; i++)
            {
                if (i == 0 || !lin[i - 1].Equals(lin[i]))
                {
                    if (defs.Count > 0)
                    {
                        if (values[i].Equals("id") && values[i + 1].Equals("("))
                        Console.WriteLine("id");
                        i++;
                        if (values[i].Equals("("))
                        {
                            Console.WriteLine("(");
                            i++;
                            while (!values[i].Equals(")"))
                            {
                                if (values[i].Equals("id"))
                                {
                                    Console.WriteLine("id");
                                    i++;
                                    if (values[i].Equals(","))
                                    {
                                        Console.WriteLine(",");
                                        i++;
                                    }
                                    else
                                    {
                                        if (!values[i].Equals(")"))
                                        {
                                            Error(i);
                                            break;
                                        }
                                    }
                                }
                            }

                            Console.WriteLine(")");
                            i++;

                            if (i > values.Count - 1) break;
                        }

                        
                    }
                    if (values[i].Equals("int") || values[i].Equals("float") || values[i].Equals("string"))
                    {
                        Console.WriteLine("int|float|string");
                        i++;
                        if (values[i].Equals("id"))
                        {
                            Console.WriteLine("id");
                            i++;

                            i = attribution(i);

                            if (i == -1) break;
                        }
                        else
                        {
                            Error(i);

                            break;
                        }
                    }
                    else if (values[i].Equals("if") || values[i].Equals("while"))
                    {
                        Console.WriteLine("if | while");

                        if (values[i].Equals("while"))
                        {
                            whiles.Add(col[i]);
                        } else
                        {
                            ifs.Add(col[i]);
                        }

                        i++;

                        i = regularExpression(i);

                        if (i == -1) break;

                        while (values[i].Equals("and") || values[i].Equals("or")) {
                            if (values[i].Equals("and") || values[i].Equals("or"))
                            {
                                i++;
                                Console.WriteLine("and | or");

                                i = regularExpression(i);

                                if (i == -1) break;
                            }
                        }

                        if (values[i].Equals(":"))
                        {
                            Console.WriteLine(":");
                        } else
                        {
                            Error(i);

                            break;
                        }
                    }
                    else if (values[i].Equals("else") || values[i].Equals("elif"))
                    {
                        Console.WriteLine("else | elif");
                        if (ifs.Count > 0 || whiles.Count > 0 || fors.Count > 0)
                        {
                            bool founded = false;

                            if (values[i].Equals("elif"))
                            {

                                for (int j = 0; j < ifs.Count; j++)
                                {
                                    if (ifs[j].Equals(col[i]))
                                    {
                                        founded = true;

                                        i++;

                                        i = regularExpression(i);

                                        if (i == -1) break;

                                        while (values[i].Equals("and") || values[i].Equals("or"))
                                        {
                                            if (values[i].Equals("and") || values[i].Equals("or"))
                                            {
                                                i++;
                                                Console.WriteLine("and | or");

                                                i = regularExpression(i);

                                                if (i == -1) break;

                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int j = 0; j < ifs.Count; j++)
                                {
                                    if (ifs[j].Equals(col[i]))
                                    {
                                        founded = true;

                                        ifs.RemoveAt(j);
                                        i++;
                                    }
                                }

                                for (int j = 0; j < whiles.Count; j++)
                                {
                                    if (whiles[j].Equals(col[i]))
                                    {
                                        founded = true;

                                        whiles.RemoveAt(j);
                                        i++;
                                    }
                                }

                                for (int j = 0; j < fors.Count; j++)
                                {
                                    if (fors[j].Equals(col[i]))
                                    {
                                        founded = true;

                                        fors.RemoveAt(j);
                                        i++;
                                    }
                                }
                            }

                            if (!founded)
                            {
                                Error(i);

                                break;
                            }
                            else if (!values[i].Equals(":"))
                            {
                                Error(i);

                                break;
                            }
                            else
                            {
                                Console.WriteLine(":");
                            }
                        } else
                        {
                            Error(i + 1);

                            break;
                        }
                    } else if (values[i].Equals("for"))
                    {
                        Console.WriteLine("for");
                        i++;
                        if (values[i].Equals("id"))
                        {
                            Console.WriteLine("id");
                            i++;
                            if (values[i].Equals("in"))
                            {
                                Console.WriteLine("in");
                                i++;
                                if (values[i].Equals("id"))
                                {
                                    Console.WriteLine("id");
                                    i++;
                                    if (values[i].Equals(":"))
                                    {
                                        Console.WriteLine(":");
                                        fors.Add(col[i]);
                                    }
                                    else
                                    {
                                        Error(i);

                                        break;
                                    }
                                }
                                else
                                {
                                    Error(i);

                                    break;
                                }
                            }
                            else
                            {
                                Error(i);

                                break;
                            }

                        } else
                        {
                            Error(i);

                            break;
                        }
                    }
                    else if (values[i].Equals("def"))
                    {
                        defs.Add(values[i]);
                        Console.WriteLine("def");
                        i++;
                        if (values[i].Equals("id"))
                        {
                            Console.WriteLine("id");
                            i++;
                            if (values[i].Equals("("))
                            {
                                Console.WriteLine("(");
                                i++;
                                while (!values[i].Equals(")"))
                                {
                                    if (values[i].Equals("id"))
                                    {
                                        Console.WriteLine("id");
                                        i++;
                                        if (values[i].Equals(","))
                                        {
                                            Console.WriteLine(",");
                                            i++;
                                        }
                                        else
                                        {
                                            if (!values[i].Equals(")"))
                                            {
                                                Error(i);
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine(")");
                                            }
                                        }
                                    }                                  
                                }

                                i++;
                                if (!values[i].Equals(":"))
                                {
                                    Error(i);
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine(":");
                                }
                            }
                            else
                            {
                                Error(i);
                                break;
                            }
                        }
                        else
                        {
                            Error(i);
                            break;
                        }
                    }
                    else if (values[i].Equals("return"))
                    {
                        Console.WriteLine("return");
                        i++;
                        if (!values[i].Equals("id"))
                        {
                            Error(i);
                            break;
                        } else
                        {
                            Console.WriteLine("id");

                            i = operations(i);

                            if (i == -1) break;
                        }
                    }
                    else if (values[i].Equals("oct") || values[i].Equals("hex") || values[i].Equals("bin") ||
                        values[i].Equals("dec"))
                    {
                        Console.WriteLine("oct | hex | bin | dec");
                        i++;
                        if (values[i].Equals("("))
                        {
                            Console.WriteLine("(");
                            i++;

                            if (values[i].Equals("id"))
                            {
                                Console.WriteLine("id");
                                i++;

                                if (!values[i].Equals(")"))
                                {
                                    Error(i);

                                    break;
                                }
                                else
                                {
                                    Console.WriteLine(")");
                                }
                            }
                        }
                        else
                        {
                            Error(i);

                            break;
                        }
                    }
                    else if (values[i].Equals("id"))
                    {
                        i++;
                        Console.WriteLine("id");

                        int j = bitTobit(i);

                        if (j == -1 )
                        {
                            i = attribution(i);
                        } else
                        {
                            i = j;
                        }

                        if (i == -1) break;

                    }
                    else if (values[i].Equals("~"))
                    {
                        i++;
                        Console.WriteLine("~");

                        if (values[i].Equals("id"))
                        {
                            Console.WriteLine("id");
                        }
                        else
                        {
                            Error(i);

                            break;

                        }
                    }
                    else
                    {
                        Error(i);

                        break;
                    }
                }
                else
                {
                    Error(i);

                    break;
                }
            }

            if (IsCorrect)
            {
                Console.WriteLine();
                Console.WriteLine("________________________");
                Console.WriteLine();

                Console.WriteLine("Sintaxe correta !");
                Console.WriteLine("________________________");
                Console.WriteLine();

            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("________________________");
                Console.WriteLine("Sintaxe Incorreta !");
                Console.WriteLine("________________________");
                Console.WriteLine();
            }
        }

        public int bitTobit(int i)
        {
            if (i == -1 || i >= values.Count)
                return -1;

            if (values[i].Equals(">>") || values[i].Equals("<<") || values[i].Equals("&") || values[i].Equals("|"))
            {
                i++;
                Console.WriteLine(">> | << | & | |");

                if (values[i].Equals("id"))
                {
                    Console.WriteLine("id");

                    return i;
                }
                else
                {
                    Error(i);

                    return -1;
                }             
            }

            return -1;
        }

        public int operations(int i)
        {
            if (i == -1 || i >= values.Count)
                return -1;

            if (values[i].Equals("+") || values[i].Equals("-") || values[i].Equals("*") || values[i].Equals("/") || values[i].Equals("^"))
            {
                i++;
                Console.WriteLine("+ | - | * | /");

                if (values[i].Equals("id"))
                {
                    Console.WriteLine("id");

                    int k = bitTobit(i + 1);
                    if (k != -1) i = k;

                    return i;
                }
                else
                {
                    Error(i);

                    return -1;
                }
            } else
            {
                return -1;
            }
        }

        public int attribution(int i)
        {
            if (values[i].Equals("="))
            {
                i++;
                Console.WriteLine("=");

                if (values[i].Equals("id"))
                {
                    Console.WriteLine("id");

                    int j = i;
                    while (j != -1 || j > values.Count)
                    {
                        j++;

                        j = operations(j);

                        if (j != -1) i = j;
                    }

                    return i;                                      
                }
                else
                {
                    Error(i);

                    return -1;
                }
            }
            else if (values[i].Equals("++") || values[i].Equals("--"))
            {
                Console.WriteLine("++ | --");

                return i;
            }
            else
            {
                Error(i);

                return -1;
            }
        }

        public int regularExpression(int i)
        {
            if (values[i].Equals("id"))
            {
                i++;
                Console.WriteLine("id");

                int j = operations(i);
                if (j != -1) i = j + 1;

                if (values[i].Equals("<") || values[i].Equals(">") || values[i].Equals("<=") ||
                    values[i].Equals(">=") || values[i].Equals("==") || values[i].Equals("!="))
                {
                    i++;
                    Console.WriteLine("< | > | <= | >= | == | !=");
                    
                    if (values[i].Equals("id"))
                    {
                        i++;
                        Console.WriteLine("id");

                        return i;
                    }
                    else
                    {
                        Error(i);
                    }
                }
                else if (values[i].Equals("%"))
                {
                    Console.WriteLine("%");
                    i++;
                    if (values[i].Equals("id"))
                    {
                        Console.WriteLine("id");

                        i++;
                        if (values[i].Equals("="))
                        {
                            Console.WriteLine("=");
                            i++;
                            if (values[i].Equals("id"))
                            {
                                i++;
                                Console.WriteLine("id");

                                return i;
                            }
                            else
                            {
                                Error(i);
                            }
                        }
                        else
                        {
                            Error(i);
                        }
                    }
                    else
                    {
                        Error(i);
                    }
                }
                
            }
            else
            {
                Error(i);
            }

            return -1;
        }

        public void Error(int index)
        {
            System.Console.WriteLine("ERRO:  Lexema: " + values[index] + "  |  Linha: " + lin[index]
                + "  |  Coluna: " + col[index]);

            IsCorrect = false;
        }
    }
}
